<?php

/**
 * Function para ler o arquivo json e fazer a chamada de quais
 * notas serão sacadas
 */
function GetNotas($retirado){
    $file = "data.json";
    $notas = json_decode(file_get_contents($file));
    return VerificaNotas($notas,$retirado);
}

/**
 *Recebe as notas existentes no caixa eletrônico e retorna quais foram
 * sacadas, caso o valor seja inexistente ou incorreto irá retornar
 * um aviso para o usuário
 * 
 * @param array $notas Notas presente no arquivo json
 * @param string $retirado Valor que o usário deseja sacar
 */
function VerificaNotas($notas, $retirado){
    $texto_retorno = "Saque de: <br/>";
    $valor_atual = $retirado;
    //Laço para verificar quais notas serão sacadas
    foreach($notas as $n){
        $contem_nota = true;
	$count = 0;
	while($contem_nota){
            if($valor_atual > 0){
                $verifica_valor = $valor_atual - $n->valor;
                if($verifica_valor >= 0){
                    $valor_atual = $verifica_valor;
                    $count++;
	  	}else{
                    $contem_nota = false;
	  	}
            }else{
                $contem_nota = false;
            }
	}
        if($count > 0){
            $texto_retorno = 
                $texto_retorno . $count . " de R$" . $n->valor . "<br />";
	}
    }
    //Verificação se os valores estão corretos para o saque
    if($valor_atual > 0){
        $texto_retorno = 
            "Infelizmente n&atilde;o temos notas para esse tipo de transa&ccedil;&atilde;o";
    }else if(!is_numeric($valor_atual) || $valor_atual != 0){
        $texto_retorno = "Por favor digite um valor v&aacute;lido";
    }
    return $texto_retorno;
}